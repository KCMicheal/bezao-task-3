﻿using System;
using System.Text;

namespace BEZAO___Task_3.Helpers
{
    public class TextBuilder
    {
        private static StringBuilder _sb;

        public static String DisplayMenuContent()
        {
            _sb = new StringBuilder().AppendLine();
            _sb.Append("Welcome to the BEZAO_Task_3 Banking Console APP");
            _sb.AppendLine();
            _sb.Append("-----------------------------------------------");
            _sb.AppendLine();
            _sb.Append("Select an Option to get Started:");
            _sb.AppendLine();
            _sb.Append("1. Create An Account");
            _sb.AppendLine();
            _sb.Append("2. Check Balance");
            _sb.AppendLine();
            _sb.Append("3. Make A deposit");
            _sb.AppendLine();
            _sb.Append("4. Withdraw ");
            _sb.AppendLine();
            _sb.Append("5. Check your Transaction History");
            _sb.AppendLine();
            _sb.Append("6. Check All Customers Transaction History");
            _sb.AppendLine();
            _sb.Append("7. Exit");
            _sb.AppendLine();
            _sb.Append("-----------------------------------------------");

            return _sb.ToString();
        }

        public static string Prompt(String text)
        {
            _sb = new StringBuilder().AppendLine();
            _sb.Append("-----------------------------------------------");
            _sb.AppendLine();
            _sb.Append(text);
            _sb.AppendLine();
            _sb.Append("-----------------------------------------------");

            return _sb.ToString();

        }
    }
}