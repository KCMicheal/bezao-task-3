﻿using System;
using BEZAO___Task_3.Interfaces;
using BEZAO___Task_3.Models;
using BEZAO___Task_3.Services;

namespace BEZAO___Task_3.Persistence
{
    public class TransactionRepository
    {
        private readonly ILogger _logger;

        private readonly BankContext _dbContext = new BankContext();


        public TransactionRepository()
        {
            _logger = new Logger();
        }


        public void AddCredit(object sender, CustomerInfoArgs e)
        {
            var newTransaction = new Transaction()
            {
                Time = DateTime.Now,
                TransactionType = e.TransactionType,
                CustomerId = e.Customer.Id,
                Amount = e.Amount,
                Remark = $"Transaction: {e.Alert}\nAmount: NGN{e.Amount:N}\nBalance: NGN{e.Customer.Balance:N}"
            };
            _logger.Log("Processing....");
            _dbContext.Transactions.Add(newTransaction);
            _dbContext.SaveChanges();

            _logger.Log($"\nDear, {e.Customer.FullName},\nYour Account: {e.Customer.AccountNumber} " +
                        $"has been {e.Alert}ed with NGN{e.Amount:N},\nYour New Account Balance is " +
                        $"NGN{e.Customer.Balance:N}\nDate: {newTransaction.Time:g}");
        }
    }
}