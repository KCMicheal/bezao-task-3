﻿using System;
using BEZAO___Task_3.Helpers;

namespace BEZAO___Task_3.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public TransactionType TransactionType { get; set; }
        public string Remark { get; set; }
        public Customer Customer { get; set; }
        public double Amount { get; set; }
        public int CustomerId { get; set; }

        public Transaction()
        {
            
        }

      
    }
}