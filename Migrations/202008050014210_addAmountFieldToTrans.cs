﻿namespace BEZAO___Task_3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAmountFieldToTrans : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transactions", "Amount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transactions", "Amount");
        }
    }
}
