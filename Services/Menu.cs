﻿using System;
using BEZAO___Task_3.Helpers;
using BEZAO___Task_3.Interfaces;

namespace BEZAO___Task_3.Services
{
    public class Menu
    {
        private readonly ILogger _logger;
        private string _choice;

        private readonly string _menuContent;

        
        public Menu(ILogger logger)
        {
            _logger = logger;
            _menuContent = TextBuilder.DisplayMenuContent();

        }
        public void Run()
        {
            _logger.Log(_menuContent);

            _choice = Console.ReadLine();

            MenuOptions(_choice);

        }
        private void MenuOptions(string choice)
        {
            BankActions actions = new BankActions(new Logger());
            switch (choice)
            {
                case "1":
                    actions.CreateAccount();
                   break;
                case "2":
                    actions.CheckBalance();
                    break;
                case "3":
                    actions.Deposit();
                    break;
                case "4":
                    actions.Withdraw();
                    break;
                case "5":
                    actions.GetCustomerTransactions();
                    break;
                case "6":
                    actions.GetAllTransactions();
                    break;
                case "7":
                    _logger.Log("Thanks for Banking With Us!!!");
                    break;
                default:
                    _logger.Log("invalid Input....Try Again!!!");
                    break;
                    // 7617091058
            }
        }

    }
}